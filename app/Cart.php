<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model {

    protected $table = "carts";


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products() {
        return $this->hasMany("App\CartProduct");
    }
}
