<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartProduct;
use App\Product;
use App\Http\Resources\ProductCollection;
use \App\Http\Resources\Cart as CartResource;

class CartController extends Controller
{
    /**
     * Create a new cart
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function newCart()
    {
        try {
            $cart = new Cart;
            $cart->saveOrFail();
            $response = $cart;
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }
        return response()->json($response, 201);
    }

    public function getCartProducts(string $cartId)
    {
        try {
            $cart = Cart::where('id', $cartId)->first();
            if (!($cart instanceof Cart)) {
                throw new \Exception('Cart does not exist!');
            }
            $response = new CartResource($cart);
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }

    /**
     * Add product to cart
     *
     * @param string $cartId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addProduct(string $cartId)
    {
        try {
            $productId = $this->request->get('product');
            $quantity = $this->request->get('quantity');

            $cart = Cart::where('id', $cartId)->first();
            if (!($cart instanceof Cart)) {
                throw new \Exception('Cart does not exist!');
            }

            $product = Product::where('id', $productId)->first();
            if (!($product instanceof Product)) {
                throw new \Exception('Product does not exist!');
            }

            $cartProduct = new CartProduct([
                'quantity' => $quantity,
                'product_id' => $productId,
                'cart_id' => $cartId
            ]);
            $cartProduct->saveOrFail();
            $response = $cart;

        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response, 201);
    }

    /**
     * Remove product from cart
     *
     * @param string $cartProductId
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeProduct(string $cartProductId)
    {
        try {
            $cartProduct = CartProduct::where('id', $cartProductId)->first();
            if (!($cartProduct instanceof CartProduct)) {
                throw new \Exception('Product does not exist!');
            }
            $cartProduct->delete();
            $response = ['status' => 'success'];
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }

}
