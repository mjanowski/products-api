<?php

namespace App\Http\Controllers;

use App\Product;
use App\Http\Resources\ProductCollection;

class ProductsController extends Controller
{
    /**
     * Get listing of existing products
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts()
    {
        try {
            $response = [
                'products' => new ProductCollection(Product::all()),
            ];
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }

    /**
     * Add a new product to database
     *
     * @return \Illuminate\Http\JsonResponse
     * @throws \Throwable
     */
    public function addProduct()
    {
        try {
            $product = new Product([
                'title' => $this->request->get('title'),
                'description' => $this->request->get('description'),
                'price' => $this->request->get('price')
            ]);
            $product->saveOrFail();
            $response = $product;

        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response, 201);
    }

    /**
     * Remove product from database
     *
     * @param string $productId
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeProduct(string $productId)
    {
        try {
            $product = Product::where('id', $productId)->first();
            if (!($product instanceof Product)) {
                throw new \Exception('Product does not exist!');
            }
            $product->delete();
            $response = ['status' => 'success'];
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }

    /**
     * Update product
     *
     * @param string $productId
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProduct(string $productId)
    {
        try {
            $product = Product::where('id', $productId)->first();
            if (!($product instanceof Product)) {
                throw new \Exception('Product does not exist!');
            }
            $product->update($this->request->all());
            $response = $product;
        } catch (\Exception $exception) {
            $response = [
                'status' => 'error',
                'message' => $exception->getMessage()
            ];
        }

        return response()->json($response);
    }
}
