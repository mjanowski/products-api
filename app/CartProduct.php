<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartProduct extends Model
{

    protected $fillable = ["quantity", "cart_id", "product_id"];

    protected $table = "cart_products";

    public static $rules = [
        "quantity" => "required"
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cart() {
        return $this->belongsTo("App\Cart");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product() {
        return $this->hasOne("App\Product");
    }
}
