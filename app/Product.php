<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ["title", "description", "price"];

    protected $table = "products";

    public static $rules = [
        "title" => "required",
        "description" => "required",
        "price" => "required",
    ];
}

