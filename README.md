# Products API

Simple Products / Cart API
## Prerequirements

To build & run this project you have to make sure that you have `Docker` and `Docker Compose` installed.

### Create .env file
Before you start please create .env file based on .env.example and provide the necessary variables.

`cp .env.example .env`

## Build & run containers
Build image `docker-compose build` 

Run containers in detached mode `docker-compose up -d`

### Migrate database
```
./artisan migrate:fresh
```

### Endpoints collection
[Postman endpoints collection](products-api.postman_collection.json)
