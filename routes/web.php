<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/products','ProductsController@getProducts');
$router->put('/products','ProductsController@addProduct');
$router->delete('/products/{productId}','ProductsController@removeProduct');
$router->post('/products/{productId}','ProductsController@updateProduct');

$router->get('/cart/new','CartController@newCart');
$router->get('/cart/{cartId}','CartController@getCartProducts');
$router->put('/cart/{cartId}','CartController@addProduct');
$router->delete('/cart/{cartProductId}','CartController@removeProduct');
