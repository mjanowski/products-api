<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Initial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function(Blueprint $table) {
            $table->integer('id',true)->unique();
            $table->index('id');
            $table->timestamps();
        });

        Schema::create('products', function(Blueprint $table) {
            $table->integer('id',true)->unique();
            $table->index('id');
            $table->string('title');
            $table->longText('description');
            $table->decimal('price');
            $table->timestamps();
        });

        Schema::create('cart_products', function(Blueprint $table) {
            $table->integer('id',true)->unique();
            $table->index('id');
            $table->integer('quantity');
            $table->integer('product_id');
            $table->integer('cart_id');
            $table->foreign('product_id')
                ->references('id')
                ->on('products')
                ->onDelete('cascade');
            $table->foreign('cart_id')
                ->references('id')
                ->on('carts')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
